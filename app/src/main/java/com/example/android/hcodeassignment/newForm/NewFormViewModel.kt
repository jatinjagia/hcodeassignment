package com.example.android.hcodeassignment.newForm

import android.app.Application
import android.text.TextUtils
import android.util.Log
import android.util.Patterns
import androidx.databinding.ObservableField
import androidx.lifecycle.AndroidViewModel
import androidx.lifecycle.LiveData
import androidx.lifecycle.MutableLiveData
import androidx.work.*
import com.example.android.hcodeassignment.HcodeApplication
import com.example.android.hcodeassignment.R
import com.example.android.hcodeassignment.data.Form
import com.example.android.hcodeassignment.data.HcodeLocalDatabase
import com.example.android.hcodeassignment.data.getDatabase
import com.example.android.hcodeassignment.network.HcodeApi
import com.example.android.hcodeassignment.worker.HcodeWorker
import kotlinx.coroutines.*
import okhttp3.MediaType
import okhttp3.RequestBody
import org.json.JSONObject
import java.lang.Exception

class NewFormViewModel(private val database: HcodeLocalDatabase, application: Application) :
    AndroidViewModel(application) {

    private var formId: Long = 0
    private lateinit var name: String
    private lateinit var email: String
    private lateinit var age: String
    private lateinit var serverSynced: String

    private val workManager = WorkManager.getInstance()

    private val viewModelJob = Job()
    private val coroutineScope = CoroutineScope(viewModelJob + Dispatchers.Main)


    private val _nameText = ObservableField<String>()
    val nameText: ObservableField<String>
        get() = _nameText

    private val _emailText = ObservableField<String>()
    val emailText: ObservableField<String>
        get() = _emailText

    private val _ageText = ObservableField<String>()
    val ageText: ObservableField<String>
        get() = _ageText

    private val _showToast = MutableLiveData<Int>()
    val showToast: LiveData<Int>
        get() = _showToast

    private val _navigateToShowFragment = MutableLiveData<Boolean>()
    val navigateToShowFragment: LiveData<Boolean>
        get() = _navigateToShowFragment

    fun postFormData() {
        coroutineScope.launch {
            if (validateFields()) {
                name = nameText.get().toString()
                email = emailText.get().toString()
                age = ageText.get().toString()
                val jsonObject = JSONObject()
                jsonObject.put("name", name)
                jsonObject.put("email", email)
                jsonObject.put("age", age)
                val requestBody: RequestBody =
                    RequestBody.create(MediaType.parse("application/json"), jsonObject.toString())
                try {
                    val response = HcodeApi.retrofitService.postForm(requestBody).await()
                    serverSynced = if (!response.isSuccessful) {
                        "false"
                    } else
                        "true"
                    saveToDatabase()

                } catch (e: Exception) {
                    serverSynced = "false"
                    saveToDatabase()
                }
            }
        }
    }

    private fun saveToDatabase() {
        coroutineScope.launch {
            formId = insertFormInDatabase()
            if (serverSynced=="false")
                sendToServerLater()
            Log.d("formIdLog", formId.toString())
        }
        startFragmentNavigation()
    }

    private fun startFragmentNavigation() {
        _navigateToShowFragment.value = true
    }

    fun doneNavigation() {
        _navigateToShowFragment.value = false
    }

    private suspend fun insertFormInDatabase(): Long {
        return withContext(Dispatchers.IO) {
            val form = Form()
            form.name = name
            form.age = age
            form.email = email
            form.serverSynced = serverSynced
            val formId = database.hcodeLocalDatabaseDao.insert(form)
            formId
        }
    }


    private fun validateFields(): Boolean {
        if (TextUtils.isEmpty(nameText.get()) || TextUtils.isEmpty(emailText.get()) || TextUtils.isEmpty(ageText.get())) {
            _showToast.value = R.string.empty_string_error
            return false
        } else if (!Patterns.EMAIL_ADDRESS.matcher(emailText.get()).matches()) {
            _showToast.value = R.string.email_format_validation_error
            return false
        } else if (ageText.get() == "0") {
            _showToast.value = R.string.age_error
            return false
        }
        return true
    }

    private fun sendToServerLater() {
        val constraints = Constraints.Builder()
            .setRequiredNetworkType(NetworkType.CONNECTED)
            .build()

        val workerRequest = OneTimeWorkRequestBuilder<HcodeWorker>()
            .setConstraints(constraints)
            .setInputData(createInputDataForWorker())
            .build()
        workManager.enqueue(workerRequest)
    }


    private fun createInputDataForWorker(): Data {
        val builder = Data.Builder()
        builder.putString("name", name)
        builder.putString("email", email)
        builder.putString("age", age)
        builder.putLong("formId", formId)
        return builder.build()
    }

    override fun onCleared() {
        super.onCleared()
        viewModelJob.cancel()
    }
}