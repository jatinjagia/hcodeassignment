package com.example.android.hcodeassignment.showForms

import android.app.Application
import androidx.lifecycle.LiveData
import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.ViewModel
import com.example.android.hcodeassignment.data.Form
import com.example.android.hcodeassignment.data.HcodeLocalDatabase

class ShowFormsViewModel(private val database: HcodeLocalDatabase,application: Application) :ViewModel() {


    private val _forms = database.hcodeLocalDatabaseDao.getAllForms("true")
    val forms: LiveData<List<Form>>
        get() = _forms

    private val _navigateToNewForm = MutableLiveData<Boolean>()
    val navigateToNewForm : LiveData<Boolean>
        get() = _navigateToNewForm


    fun startNewFormNavigation(){
        _navigateToNewForm.value= true
    }

    fun doneNewFormNavigation(){
        _navigateToNewForm.value= false
    }
}