package com.example.android.hcodeassignment.showForms

import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.fragment.app.Fragment
import androidx.lifecycle.Observer
import androidx.lifecycle.ViewModelProviders
import androidx.navigation.fragment.findNavController
import com.example.android.hcodeassignment.data.getDatabase
import com.example.android.hcodeassignment.databinding.FragmentShowFormsBinding

class ShowFormsFragment : Fragment(){

    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?, savedInstanceState: Bundle?): View? {

        val binding = FragmentShowFormsBinding.inflate(inflater,container,false)

        val application = (this.activity)!!.application
        val database = getDatabase(application)
        val viewModelFactory = ShowFormsViewModelFactory(database,application)
        val viewModel = ViewModelProviders.of(this,viewModelFactory).get(ShowFormsViewModel::class.java)
        binding.viewModel=viewModel
        binding.lifecycleOwner= this

        val adapter = ShowFragmentAdapter()
        binding.recyclerView.adapter= adapter

        viewModel.forms.observe(this, Observer {
            it?.let {
                adapter.submitList(it)
            }
        })

        viewModel.navigateToNewForm.observe(this, Observer {
             if(it) {
                 this.findNavController()
                     .navigate(ShowFormsFragmentDirections.actionShowFormsFragmentToNewFormFragment())
                 viewModel.doneNewFormNavigation()
             }
        })

        return binding.root
    }
}