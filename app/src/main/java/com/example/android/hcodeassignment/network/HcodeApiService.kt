package com.example.android.hcodeassignment.network

import com.example.android.hcodeassignment.data.Form
import com.jakewharton.retrofit2.adapter.kotlin.coroutines.CoroutineCallAdapterFactory
import com.squareup.moshi.Moshi
import com.squareup.moshi.kotlin.reflect.KotlinJsonAdapterFactory
import kotlinx.coroutines.Deferred
import okhttp3.RequestBody
import retrofit2.Call
import retrofit2.Retrofit
import retrofit2.converter.moshi.MoshiConverterFactory
import retrofit2.http.Body
import retrofit2.http.POST

interface HcodeApiService{
    @POST("850a9619-7490-43ba-8e8e-83ca77378954")
    fun postForm(@Body request: RequestBody) : Deferred<retrofit2.Response<Unit>>

    @POST("850a9619-7490-43ba-8e8e-83ca77378954")
    fun postAllForms(@Body request: RequestBody): Call<List<Form>>
}

private const val BASE_URL = "https://webhook.site/"

private val moshi = Moshi.Builder()
    .add(KotlinJsonAdapterFactory()).build()

private val retrofit = Retrofit.Builder()
    .addConverterFactory(MoshiConverterFactory.create(moshi))
    .addCallAdapterFactory(CoroutineCallAdapterFactory())
    .baseUrl(BASE_URL)
    .build()

object HcodeApi{
    val retrofitService : HcodeApiService by lazy {
        retrofit.create(HcodeApiService::class.java)
    }
}