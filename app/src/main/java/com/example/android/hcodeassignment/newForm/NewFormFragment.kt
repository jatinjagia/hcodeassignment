package com.example.android.hcodeassignment.newForm

import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.Toast
import androidx.fragment.app.Fragment
import androidx.lifecycle.Observer
import androidx.lifecycle.ViewModelProvider
import androidx.lifecycle.ViewModelProviders
import androidx.navigation.fragment.findNavController
import com.example.android.hcodeassignment.data.getDatabase
import com.example.android.hcodeassignment.databinding.FragmentFormBinding

class NewFormFragment :Fragment(){

    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?, savedInstanceState: Bundle?): View? {
        val binding= FragmentFormBinding.inflate(inflater,container,false)

        val application = requireNotNull(this.activity).application
        val database = getDatabase(application)
        val viewModelFactory = NewFormViewModelFactory(database,application)
        val viewModel = ViewModelProviders.of(this,viewModelFactory).get(NewFormViewModel::class.java)

        binding.formViewModel = viewModel
        binding.lifecycleOwner=this


        viewModel.showToast.observe(this, Observer { toastMessage->
            Toast.makeText(context,getString(toastMessage),Toast.LENGTH_SHORT).show()
        })

        viewModel.navigateToShowFragment.observe(this, Observer {
            if (it) {
                this.findNavController()
                    .navigate(NewFormFragmentDirections.actionNewFormFragmentToShowFormsFragment())
                viewModel.doneNavigation()
            }
        })

        return binding.root
    }
}