package com.example.android.hcodeassignment.worker

import android.content.Context
import androidx.work.CoroutineWorker
import androidx.work.WorkerParameters
import com.example.android.hcodeassignment.data.getDatabase
import com.example.android.hcodeassignment.network.HcodeApi
import okhttp3.MediaType
import okhttp3.RequestBody
import org.json.JSONObject
import retrofit2.HttpException
import java.lang.Exception

class HcodeWorker(appContext: Context, params: WorkerParameters) : CoroutineWorker(appContext, params) {


    override suspend fun doWork(): Result {
        val database = getDatabase(applicationContext)
        val formId = inputData.getLong("formId", 0)
        val jsonObject = JSONObject()
        jsonObject.put("name", inputData.getString("name"))
        jsonObject.put("email", inputData.getString("email"))
        jsonObject.put("age", inputData.getString("age"))

        return try {
            val requestBody = RequestBody.create(MediaType.parse("application/json"), jsonObject.toString())
            try {
                val response = HcodeApi.retrofitService.postForm(requestBody).await()
                if (response.isSuccessful) {
                    val form = database.hcodeLocalDatabaseDao.get(formId)
                    form?.serverSynced="true"
                    form?.let { database.hcodeLocalDatabaseDao.update(it) }
                    Result.success()
                } else
                    Result.retry()
            } catch (e: Exception) {
                Result.retry()
            }
        } catch (e: HttpException) {
            Result.retry()
        }
    }

}