package com.example.android.hcodeassignment

import android.text.TextUtils
import android.util.Patterns
import androidx.databinding.BindingAdapter
import com.google.android.material.textfield.TextInputLayout

@BindingAdapter("errorText")
fun setErrorMessaged(view: TextInputLayout, editTextString: String?) {
    val minLength = 3
    if (TextUtils.isEmpty(editTextString)) {
        view.error = view.resources.getString(R.string.empty_string_error)
    } else if (view.id == R.id.textInputLayout) {
        if (!Patterns.EMAIL_ADDRESS.matcher(editTextString).matches())
            view.error = view.resources.getString(R.string.email_format_validation_error)
    } else if (view.id == R.id.textInputLayout4) {
        if (editTextString == "0")
            view.error = view.resources.getString((R.string.age_error))
    }
}
