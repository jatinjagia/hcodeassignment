package com.example.android.hcodeassignment.newForm

import android.app.Application
import androidx.lifecycle.ViewModel
import androidx.lifecycle.ViewModelProvider
import com.example.android.hcodeassignment.HcodeApplication
import com.example.android.hcodeassignment.data.HcodeLocalDatabase
import java.lang.IllegalArgumentException

class NewFormViewModelFactory (private val database: HcodeLocalDatabase,private val application: Application) : ViewModelProvider.Factory{

    @Suppress("unchecked_cast")
    override fun <T:ViewModel?> create(modelClass: Class<T>): T{
        if  (modelClass.isAssignableFrom(NewFormViewModel::class.java)){
            return NewFormViewModel(database,application) as T
        }
        throw IllegalArgumentException("unKnown ViewModel Class")
    }
}