package com.example.android.hcodeassignment.data

import androidx.lifecycle.LiveData
import androidx.room.Dao
import androidx.room.Insert
import androidx.room.Query
import androidx.room.Update

@Dao
interface HcodeLocalDatabaseDao{

    @Insert
    fun insert(form: Form) : Long

    @Update
    fun update(form:Form)

    @Query("SELECT * from form_table WHERE formId = :key")
    fun get(key: Long): Form?

    @Query("SELECT * from form_table WHERE server_synced= :serverSyncedValue ORDER BY formId DESC ")
    fun getAllForms(serverSyncedValue: String): LiveData<List<Form>>


}
