package com.example.android.hcodeassignment.data

import android.content.Context
import androidx.room.Database
import androidx.room.Room
import androidx.room.RoomDatabase

@Database(entities = [Form::class],version = 1)
abstract class HcodeLocalDatabase : RoomDatabase() {
    abstract val hcodeLocalDatabaseDao: HcodeLocalDatabaseDao
}

private lateinit var INSTANCE: HcodeLocalDatabase

fun getDatabase(context: Context): HcodeLocalDatabase{
    synchronized(HcodeLocalDatabase::class.java){
        if (!::INSTANCE.isInitialized){
            INSTANCE = Room.databaseBuilder(context.applicationContext,
                HcodeLocalDatabase::class.java,
                "hcode_local_database").build()
        }
    }
    return INSTANCE
}
