package com.example.android.hcodeassignment.showForms

import android.view.LayoutInflater
import android.view.ViewGroup
import androidx.recyclerview.widget.DiffUtil
import androidx.recyclerview.widget.ListAdapter
import androidx.recyclerview.widget.RecyclerView
import com.example.android.hcodeassignment.data.Form
import com.example.android.hcodeassignment.databinding.FormItemBinding

class ShowFragmentAdapter : ListAdapter<Form, ShowFragmentAdapter.ViewHolder>(FormDiffCallBack()){


    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): ViewHolder {
        return ViewHolder.from(parent)
    }

    override fun onBindViewHolder(holder: ViewHolder, position: Int) {
        val item = getItem(position)
        holder.bind(item)
    }


    class ViewHolder private constructor(val binding: FormItemBinding): RecyclerView.ViewHolder(binding.root){

        fun bind(item: Form){
            binding.form=item
            binding.executePendingBindings()
        }

        companion object {
            internal fun from(parent: ViewGroup): ViewHolder {
                val layoutInflater = LayoutInflater.from(parent.context)
                val binding = FormItemBinding.inflate(layoutInflater, parent, false)
                return ViewHolder(binding)
            }
        }
    }
    class FormDiffCallBack :
        DiffUtil.ItemCallback<Form>() {
        override fun areItemsTheSame(oldItem: Form, newItem: Form): Boolean {
            return oldItem.formId == newItem.formId
        }

        override fun areContentsTheSame(oldItem: Form, newItem: Form): Boolean {
            return newItem == oldItem
        }
    }
}