package com.example.android.hcodeassignment.data

import androidx.room.ColumnInfo
import androidx.room.Entity
import androidx.room.PrimaryKey

@Entity(tableName = "form_table")
data class Form(
    @PrimaryKey(autoGenerate = true)
    var formId: Long =0L,

    @ColumnInfo(name="name")
    var name: String = "",

    @ColumnInfo(name ="email")
    var email: String = "",

    @ColumnInfo(name = "age")
    var age : String = "",

    @ColumnInfo(name = "server_synced")
    var serverSynced: String = ""
)